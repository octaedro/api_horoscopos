from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import AbstractUser

from signs.models import Sign

class CustomUser(AbstractUser):
	birthday = models.DateField(auto_now=False, auto_now_add=False)
	sign = models.ForeignKey(Sign)
	requests = models.IntegerField(default=0)

	def __unicode__(self):
		return self.content

	def __str__(self):
		return self.content

	@property
	def signs(self):
		instance = self
		qs = Sign.objects.filter_by_instance(instance)
		return qs

USERNAME_FIELD = 'email'