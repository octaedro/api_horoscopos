from django.conf.urls import url
from django.contrib import admin

from .views import (
	concept_list,
	# concept_create,
	concept_detail,
	# concept_update,
	# concept_delete,
	)

urlpatterns = [
	url(r'^$', concept_list, name='list'),
    # url(r'^create/$', concept_create),
    url(r'^(?P<pk>[\w-]+)/$', concept_detail, name='detail'),
    # url(r'^(?P<slug>[\w-]+)/edit/$', concept_update, name='update'),
    # url(r'^(?P<slug>[\w-]+)/delete/$', concept_delete),
    #url(r'^concepts/$', "<appname>.views.<function_name>"),
]
