from django.conf.urls import url
from django.contrib import admin

from .views import (
	# ConceptCreateAPIView,
	# ConceptDeleteAPIView,
	ConceptDetailAPIView,
	ConceptListAPIView,
	# ConceptUpdateAPIView
	)

urlpatterns = [
	url(r'^$', ConceptListAPIView.as_view(), name='list'),
    # url(r'^create/$', ConceptCreateAPIView.as_view(), name='create'),
    url(r'^(?P<pk>[\w-]+)/$', ConceptDetailAPIView.as_view(), name='detail'),
    # url(r'^(?P<slug>[\w-]+)/edit/$', ConceptUpdateAPIView.as_view(), name='update'),
    # url(r'^(?P<slug>[\w-]+)/delete/$', ConceptDeleteAPIView.as_view(), name='delete'),
    #url(r'^posts/$', "<appname>.views.<function_name>"),
]
