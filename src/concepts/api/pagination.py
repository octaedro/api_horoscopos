
from rest_framework.pagination import (
	LimitOffsetPagination,
	PageNumberPagination
	)

class ConceptLimitOffsetPagination(LimitOffsetPagination):
	default_limit = 10
	max_limit = 10

class ConceptPageNumberPagination(PageNumberPagination):
	page_size = 10
