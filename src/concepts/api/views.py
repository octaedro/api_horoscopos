from django.db.models import Q

from rest_framework.filters import (
	SearchFilter,
	OrderingFilter
	)

from rest_framework.generics import (
	# CreateAPIView,
	# DestroyAPIView,
	ListAPIView, 
	RetrieveAPIView,
	# RetrieveUpdateAPIView,
	# UpdateAPIView
	)

from rest_framework.permissions import (
	AllowAny,
	IsAuthenticated,
	IsAdminUser,
	IsAuthenticatedOrReadOnly
	)

from concepts.models import Concept

from .pagination import ConceptLimitOffsetPagination, ConceptPageNumberPagination
from .permissions import IsOwnerOrReadOnly
from .serializers import (
	# ConceptCreateUpdateSerializer, 
	ConceptDetailSerializer, 
	ConceptListSerializer
	)

# class ConceptCreateAPIView(CreateAPIView):
# 	queryset = Concept.objects.all()
# 	serializer_class = ConceptCreateUpdateSerializer
# 	permission_classes = [IsAuthenticated]

# 	def perform_create(self, serializer):
# 		serializer.save(user=self.request.user)


# class ConceptDeleteAPIView(DestroyAPIView):
# 	queryset = Concept.objects.all()
# 	serializer_class = ConceptDetailSerializer
# 	lookup_field = 'slug'
# 	permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

class ConceptDetailAPIView(RetrieveAPIView):
	queryset = Concept.objects.all()
	serializer_class = ConceptDetailSerializer
	lookup_field = 'slug'

class ConceptListAPIView(ListAPIView):
	serializer_class = ConceptListSerializer
	filter_backends = [SearchFilter, OrderingFilter]
	search_fields = ['id','name','publish']
	pagination_class = ConceptPageNumberPagination #PageNumberPagination

	def get_queryset(self, *args, **kwargs):
		# queryset_list = super(ConceptListAPIView, self.get_queryset(*args, **kwargs))
		queryset_list = Concept.objects.all()
		query = self.request.GET.get("q")
		if query:
			queryset_list = queryset_list.filter(
					Q(id__icontains=query)|
					Q(name__icontains=query)|
					Q(publish__icontains=query)
					).distinct()
		return queryset_list


# class ConceptUpdateAPIView(RetrieveUpdateAPIView):
# 	queryset = Concept.objects.all()
# 	serializer_class = ConceptCreateUpdateSerializer
# 	lookup_field = 'slug'
# 	permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

# 	def perform_update(self, serializer):
# 			serializer.save(user=self.request.user)
# 			#email send_email

