from rest_framework.serializers import (
	HyperlinkedIdentityField, 
	ModelSerializer, 
	SerializerMethodField
	)

from concepts.models import Concept

# class ConceptCreateUpdateSerializer(ModelSerializer):
# 	class Meta:
# 		model = Concept
# 		fields = [
# 			# 'id',
# 			'title',
# 			# 'slug',
# 			'content',
# 			'publish',
# 		]

concept_detail_url = HyperlinkedIdentityField(
		view_name = 'concepts-api:detail',
		lookup_field = 'pk'
		)

class ConceptDetailSerializer(ModelSerializer):
	# url = concept_detail_url
	# user = SerializerMethodField()
	# image = SerializerMethodField()
	# id_user = SerializerMethodField()
	# html = SerializerMethodField()
	class Meta:
		model = Concept
		fields = [
			'id',
			# 'url',
			# 'id_user',
			# 'user',
			# 'title',
			# 'slug',
			# 'content',
			# 'html',
			'name',
			'publish',
			# 'image',
		]
	# def get_user(self,obj):
	# 	return str(obj.user.username)

	# def get_id_user(self,obj):
	# 	return obj.user.id

	# def get_image(self,obj):
	# 	try:
	# 		image = obj.image.url
	# 	except:
	# 		image = None
	# 	return image

	# def get_html(self,obj):
	# 	return obj.get_markdown()

class ConceptListSerializer(ModelSerializer):
	# url = concept_detail_url
	# user = SerializerMethodField()
	class Meta:
		model = Concept
		fields = [
			'id',
			# 'url',
			# 'user',
			# 'title',
			# 'slug',
			# 'content',
			'name',
			'publish',
		]

	# def get_user(self,obj):
	# 	return str(obj.user.username)

"""
from concepts.models import Concept
from concepts.api.serializers import ConceptDetailSerializer

data = {
	"title": "Nuevo TITULO en consola",
	"content": "Nuevo CONTENIDO en consola",
	"publish": "2017-7-14",
	"slug": "slug-consola",
}

obj = Concept.objects.get(id=2)
new_item = ConceptDetailSerializer(obj, data=data)
if new_item.is_valid():
 new_item.save()
else:
 print(new_item.errors)

"""

