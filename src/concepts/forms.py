from django import forms

from pagedown.widgets import PagedownWidget

from .models import Concept


class ConceptForm(forms.ModelForm):
    content = forms.CharField(widget=PagedownWidget(show_preview=False))
    publish = forms.DateField(widget=forms.SelectDateWidget)
    class Meta:
        model = Concept
        fields = [
            # "title",
            # "content",
            # "image",
            # "draft",
            "name",
            "publish",
        ]