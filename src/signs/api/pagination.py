
from rest_framework.pagination import (
	LimitOffsetPagination,
	PageNumberPagination
	)

class SignLimitOffsetPagination(LimitOffsetPagination):
	default_limit = 12
	max_limit = 12

class SignPageNumberPagination(PageNumberPagination):
	page_size = 12
