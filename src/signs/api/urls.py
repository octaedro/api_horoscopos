from django.conf.urls import url
from django.contrib import admin

from .views import (
	# SignCreateAPIView,
	# SignDeleteAPIView,
	SignDetailAPIView,
	SignListAPIView,
	# SignUpdateAPIView
	)

urlpatterns = [
	url(r'^$', SignListAPIView.as_view(), name='list'),
    # url(r'^create/$', SignCreateAPIView.as_view(), name='create'),
    url(r'^(?P<pk>[\w-]+)/$', SignDetailAPIView.as_view(), name='detail'),
    # url(r'^(?P<slug>[\w-]+)/edit/$', SignUpdateAPIView.as_view(), name='update'),
    # url(r'^(?P<slug>[\w-]+)/delete/$', SignDeleteAPIView.as_view(), name='delete'),
    #url(r'^signs/$', "<appname>.views.<function_name>"),
]
