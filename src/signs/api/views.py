from django.db.models import Q

from rest_framework.filters import (
	SearchFilter,
	OrderingFilter
	)

from rest_framework.generics import (
	ListAPIView, 
	RetrieveAPIView,
	)

from rest_framework.permissions import (
	AllowAny,
	IsAuthenticated,
	IsAdminUser,
	IsAuthenticatedOrReadOnly
	)

from signs.models import Sign

from .pagination import SignLimitOffsetPagination, SignPageNumberPagination
from .permissions import IsOwnerOrReadOnly
from .serializers import (
	SignDetailSerializer, 
	SignListSerializer
	)


class SignDetailAPIView(RetrieveAPIView):
	queryset = Sign.objects.all()
	serializer_class = SignDetailSerializer
	permission_classes = [IsAdminUser]
	lookup_field = 'slug'

class SignListAPIView(ListAPIView):
	serializer_class = SignListSerializer
	filter_backends = [SearchFilter, OrderingFilter]
	search_fields = ['id','name','publish']
	pagination_class = SignPageNumberPagination #PageNumberPagination

	def get_queryset(self, *args, **kwargs):
		queryset_list = Sign.objects.all()
		query = self.request.GET.get("q")
		if query:
			queryset_list = queryset_list.filter(
					Q(id__icontains=query)|
					Q(name__icontains=query)|
					Q(publish__icontains=query)
					).distinct()
		return queryset_list
