from django.contrib import admin

# Register your models here.
from .models import Sign

class SignModelAdmin(admin.ModelAdmin):
	list_display = ["id", "name", "publish"]
	# list_display_links = ["updated"]
	list_editable = ["publish"]
	list_filter = ["name", "publish"]

	search_fields = ["name", "publish"]
	class Meta:
		model = Sign


admin.site.register(Sign, SignModelAdmin)