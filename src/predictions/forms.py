from django import forms

from pagedown.widgets import PagedownWidget

from .models import Prediction


class PredictionForm(forms.ModelForm):
    content = forms.CharField(widget=PagedownWidget(show_preview=False))
    publish = forms.DateField(widget=forms.SelectDateWidget)
    class Meta:
        model = Prediction
        fields = [
            # "title",
            "content",
            # "image",
            # "draft",
            "publish",
        ]