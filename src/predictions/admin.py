from django.contrib import admin

# Register your models here.
from .models import Prediction

class PredictionModelAdmin(admin.ModelAdmin):
	list_display = ["content", "concept"]
	# list_display_links = ["updated"]
	# list_editable = ["title"]
	list_filter = ["content", "concept"]

	search_fields = ["content", "concept"]
	class Meta:
		model = Prediction


admin.site.register(Prediction, PredictionModelAdmin)