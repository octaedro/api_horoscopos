from django.conf.urls import url
from django.contrib import admin

from .views import (
	prediction_list,
	prediction_create,
	# prediction_detail,
	prediction_update,
	prediction_delete,
	)

urlpatterns = [
	url(r'^$', prediction_list, name='list'),
    url(r'^create/$', prediction_create),
    # url(r'^(?P<slug>[\w-]+)/$', prediction_detail, name='detail'),
    url(r'^(?P<slug>[\w-]+)/edit/$', prediction_update, name='update'),
    url(r'^(?P<slug>[\w-]+)/delete/$', prediction_delete),
    #url(r'^predictions/$', "<appname>.views.<function_name>"),
]
