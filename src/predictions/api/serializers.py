from django.db.models import Q
from rest_framework.exceptions import APIException

from rest_framework.serializers import (
	HyperlinkedIdentityField, 
	ModelSerializer, 
	SerializerMethodField,
	ValidationError
	)

from predictions.models import Prediction
from signs.models import Sign

class PredictionCreateUpdateSerializer(ModelSerializer):
	class Meta:
		model = Prediction
		fields = [
			'content',
			'concept',
		]
	def validate(self, attrs):
		content = attrs.get("content")
		content_length = len(content)
		if content_length > 100:
			half_length = int(content_length / 2)
			half_init = half_length/2
			half_finish = (half_length - half_init) * -1
			content = content[half_init:half_finish]

		queryset_list = Prediction.objects.all()
		repeated = queryset_list.filter(
			Q(content__icontains=content)
		).distinct()

		try:
			if repeated[0]:
				raise APIException('Contenido repetido.')
		except IndexError as e:
			return attrs


class PredictionDetailSerializer(ModelSerializer):
	concept = SerializerMethodField()
	class Meta:
		model = Prediction
		fields = [
			'id',
			'content',
			'publish',
			'concept',
			'length',
		]
	def get_concept(self,obj):
		return str(obj.concept.name)

	def get_sign(self,obj):
		return str(obj.sign.name)


class PredictionListSerializer(ModelSerializer):
	class Meta:
		model = Prediction
		fields = [
			'id',
			'content',
			'timestamp',
			'concept',
			'publish',
			'length',
		]


class PredictionNewSerializer(ModelSerializer):
	class Meta:
		model = Prediction
		fields = [
			'id',
			'content',
			'concept',
			'length',
		]


class NewResponseSerializer(ModelSerializer):
	content = SerializerMethodField()
	concept = SerializerMethodField()
	publish = SerializerMethodField()
	class Meta:
		model = Prediction
		fields = [
			'id',
			'content',
			'publish',
			'concept',
			'length',
		]

	def get_concept(self,obj):
		return obj.concept

	def get_content(self,obj):
		sign_selected = Sign.objects.filter(name=self.context['sign_param'].lower()).first()	
		if not sign_selected:
			sign_selected = Sign.objects.filter(id=self.context['sign_param']).first()	
			if not sign_selected:
				raise ValidationError('Invalid SIGN. Send a number between 1-12.')
		return obj.content.replace("[signo]", sign_selected.name.upper())

	def get_publish(self,obj):
		obj.publish=True;
		obj.save()
		return obj.publish


