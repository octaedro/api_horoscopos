from django.db.models import Q

from rest_framework.filters import (
	SearchFilter,
	OrderingFilter
	)

from rest_framework.generics import (
	CreateAPIView,
	DestroyAPIView,
	ListAPIView, 
	RetrieveAPIView,
	RetrieveUpdateAPIView,
	UpdateAPIView
	)

from rest_framework.permissions import (
	AllowAny,
	IsAuthenticated,
	IsAdminUser,
	IsAuthenticatedOrReadOnly
	)

from random import randint

from predictions.models import Prediction

from .pagination import PredictionLimitOffsetPagination, PredictionPageNumberPagination
from .permissions import IsOwnerOrReadOnly
from .serializers import (
	NewResponseSerializer,
	PredictionCreateUpdateSerializer, 
	PredictionDetailSerializer, 
	PredictionListSerializer,
	PredictionNewSerializer
	)
from rest_framework.exceptions import APIException

class PredictionCreateAPIView(CreateAPIView):
	queryset = Prediction.objects.all()
	serializer_class = PredictionCreateUpdateSerializer
	permission_classes = [IsAdminUser]
	# permission_classes = [AllowAny]

	# def perform_create(self, serializer):
	# 	content = serializer.data.get("content")
	# 	content_length = len(content)
	# 	if content_length > 31:
	# 		half_length = int(content_length / 2)
	# 		half_init = half_length/2
	# 		half_finish = (half_length - half_init) * -1
	# 		content = content[half_init:half_finish]
    #
	# 	queryset_list = Prediction.objects.all()
	# 	repeated_content = queryset_list.filter(
	# 		Q(content__icontains=content)
	# 	).distinct()
	# 	if repeated_content:
	# 		raise APIException('Contenido repetido.')


class PredictionDeleteAPIView(DestroyAPIView):
	queryset = Prediction.objects.all()
	serializer_class = PredictionDetailSerializer
	lookup_field = 'pk'
	permission_classes = [IsAdminUser]

class PredictionDetailAPIView(RetrieveAPIView):
	queryset = Prediction.objects.all()
	serializer_class = PredictionDetailSerializer
	lookup_field = 'pk'
	# permission_classes = [AllowAny]


class PredictionListAPIView(ListAPIView):
	serializer_class = PredictionListSerializer
	filter_backends = [SearchFilter, OrderingFilter]
	search_fields = ['concept']
	pagination_class = PredictionPageNumberPagination #PageNumberPagination
	permission_classes = [AllowAny]

	def get_queryset(self, *args, **kwargs):
		queryset_list = Prediction.objects.all()
		query = self.request.GET.get("q")
		if query:
			queryset_list = queryset_list.filter(
					Q(concept__name__icontains=query)
					).distinct()
		return queryset_list


class PredictionUpdateAPIView(RetrieveUpdateAPIView):
	queryset = Prediction.objects.all()
	serializer_class = PredictionCreateUpdateSerializer
	lookup_field = 'pk'
	permission_classes = [IsAdminUser]

class PredictionNewAPIView(ListAPIView):
	serializer_class = PredictionNewSerializer
	filter_backends = [SearchFilter, OrderingFilter]
	search_fields = ['concept']
	permission_classes = [AllowAny]

	def get_queryset(self, *args, **kwargs):
		queryset_list = Prediction.objects.all()
		concept = self.request.GET.get("concept")
		sign = self.request.GET.get("sign")
		minlen = self.request.GET.get("minlen")
		maxlen = self.request.GET.get("maxlen")

		if not minlen:
			minlen=0

		if not maxlen:
			maxlen=140

		if concept:
			queryset_list = queryset_list.filter(
					Q(concept__id__iexact=concept)|
					Q(concept__name__iexact=concept)
					).filter(Q(publish__iexact=0)).filter(Q(length__gte=minlen,length__lte=maxlen))[:1]
		else:
			concept = randint(1, 25)
			queryset_list = queryset_list.filter(
					Q(concept__id__iexact=concept)|
					Q(concept__name__iexact=concept)
					).filter(Q(publish__iexact=0)).filter(Q(length__gte=minlen,length__lte=maxlen))[:1]

		serializer = NewResponseSerializer(queryset_list, many=True, context={'sign_param': sign})
		user = self.request.user
		user.requests += 1
		user.save()
		return serializer.data
