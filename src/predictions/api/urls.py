from django.conf.urls import url
from django.contrib import admin

from .views import (
	PredictionCreateAPIView,
	PredictionDeleteAPIView,
	PredictionDetailAPIView,
	PredictionNewAPIView,
	PredictionListAPIView,
	PredictionUpdateAPIView
	)

urlpatterns = [
	url(r'^$', PredictionListAPIView.as_view(), name='list'),
    url(r'^create/$', PredictionCreateAPIView.as_view(), name='create'),
    url(r'^new/$', PredictionNewAPIView.as_view(), name='new'),
    url(r'^(?P<pk>[\w-]+)/$', PredictionDetailAPIView.as_view(), name='detail'),
    url(r'^(?P<pk>[\w-]+)/edit/$', PredictionUpdateAPIView.as_view(), name='update'),
    url(r'^(?P<pk>[\w-]+)/delete/$', PredictionDeleteAPIView.as_view(), name='delete'),
    # url(r'^new/(?P<sign>)(?P<item>)$', PredictionCreateAPIView.as_view(), name='new'),
    # url(r'^new/$', PredictionNewAPIView.as_view(), name='new'),
    #url(r'^predictions/$', "<appname>.views.<function_name>"),
]
