
from rest_framework.pagination import (
	LimitOffsetPagination,
	PageNumberPagination
	)

class PredictionLimitOffsetPagination(LimitOffsetPagination):
	default_limit = 10
	max_limit = 10

class PredictionPageNumberPagination(PageNumberPagination):
	page_size = 10
