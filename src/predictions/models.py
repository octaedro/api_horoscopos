from __future__ import unicode_literals

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models.signals import pre_save
from django.utils import timezone
from django.utils.safestring import mark_safe
from django.utils.text import slugify

from markdown_deux import markdown
from concepts.models import Concept

from .utils import get_read_time


class PredictionManager(models.Manager):
    def active(self, *args, **kwargs):
        return super(PredictionManager, self).filter(draft=False).filter(publish__lte=timezone.now())

def upload_location(instance, filename):
    PredictionModel = instance.__class__
    new_id = PredictionModel.objects.order_by("id").last().id + 1
    return "%s/%s" %(new_id, filename)

class Prediction(models.Model):
    content = models.TextField(max_length=2000)
    timestamp = models.DateTimeField(auto_now=False, auto_now_add=True)
    concept = models.ForeignKey(Concept)
    publish = models.BooleanField(default=False)
    length =  models.IntegerField(default=0)

    objects = PredictionManager()

    def __unicode__(self):
        return self.content

    def __str__(self):
        return self.content

    class Meta:
        ordering = ["-content"]

    @property
    def concepts(self):
        instance = self
        qs = Concept.objects.filter_by_instance(instance)
        return qs

def pre_save_prediction_receiver(sender, instance, *args, **kwargs):
    if instance.content:
        instance.length = len( instance.content )
    else:
        instance.length = len( instance.content )

pre_save.connect(pre_save_prediction_receiver, sender=Prediction)










